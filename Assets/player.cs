﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class player : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButton("left"))
        {
            transform.Translate(new Vector2(-1, 0) *Time.deltaTime);
        }
        if(Input.GetButton("right"))
        {
            transform.Translate(new Vector2(1,0)* Time.deltaTime);
        }
        if (Input.GetButton("up"))
        {
            transform.Translate(new Vector2(0,1)* Time.deltaTime);
        }
        if (Input.GetButton("down"))
        {
            transform.Translate(new Vector2(0,-1)*Time.deltaTime);
        }
    }
}
